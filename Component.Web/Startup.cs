﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Component.Web.Startup))]
namespace Component.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
