﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boot.ComponentUI.Extensions
{
    public static partial class ComponentExtensions
    {
        /// <summary>
        ///  Generates a UL LI list from an IHierarchial generic Collection<T>.
        ///  Usage : E.g
        ///  var staff = new StaffHierarchy().Staff;
        ///  var Tree = staff.HierarchicalView(r => r.Departments, r => r.DepName);
        ///  return View(Tree)
        /// </summary>
        /// <typeparam name="T">The source item</typeparam>
        /// <param name="rootItems">The list</param>
        /// <param name="childrenProperty">List of children</param>
        /// <param name="itemContent">The text displayed</param>
        /// <returns>Generated treedata</returns>
        public static string HierarchicalTreeView<T>(this IEnumerable<T> rootItems, Func<T, IEnumerable<T>> childrenProperty, Func<T, string> itemContent)
        {
            var enumerable = rootItems as IList<T> ?? rootItems.ToList();
            if (rootItems == null || !enumerable.Any()) return null;

            var builder = new StringBuilder();
            builder.AppendLine("<ul>");

            foreach (var item in enumerable)
            {
                var css = childrenProperty(item).Any() ? "glyphicon glyphicon-plus-sign" : "glyphicon glyphicon-ok-circle";

                builder
                    .Append("  <li><span><i class=\"" + css + "\"> </i>" + itemContent(item) + "</span>")
                    .AppendLine("   <a href=\"\">&nbsp;")
                    .AppendLine("   </a>");

                var childContent = HierarchicalTreeView(childrenProperty(item), childrenProperty, itemContent);

                if (childContent == null) { builder.AppendLine("</li>"); continue; }

                var indented = childContent.Replace(System.Environment.NewLine, System.Environment.NewLine + "  ");
                builder.Append("  ").AppendLine(indented);
            }

            builder.Append("</ul>");
            return builder.ToString();
        }
    }
}
