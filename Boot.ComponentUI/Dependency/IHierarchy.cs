﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boot.ComponentUI.Dependency
{
    public interface IHierarchy<T>
    {
        Int32 Id { get; set; }
        string Name { get; set; }
        ICollection<T> Collection { get; set; }
    }

    public interface IHierarchial
    {
        string Construct();
    }
}
