﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boot.ComponentUI
{
    public interface IUIComponent
    {
        void Render();
    }
}
